package com.gitlab.visutrb.androsstore.di

import com.gitlab.visutrb.androsstore.App
import com.gitlab.visutrb.androsstore.di.module.common.GooglePlayApiModule
import com.gitlab.visutrb.androsstore.di.module.common.OkHttpModule
import com.gitlab.visutrb.androsstore.di.module.contributors.ActivityContributorModule
import com.gitlab.visutrb.androsstore.di.module.contributors.ServiceContributorModule
import com.gitlab.visutrb.androsstore.di.scope.ApplicationScope
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import me.visutr.store.di.modules.AppModule

@ApplicationScope
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        ActivityContributorModule::class,
        ServiceContributorModule::class,
        AppModule::class,
        OkHttpModule::class,
        GooglePlayApiModule::class
    ]
)
interface AppComponent {

    fun inject(app: App)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun app(app: App): Builder

        fun build(): AppComponent
    }
}
