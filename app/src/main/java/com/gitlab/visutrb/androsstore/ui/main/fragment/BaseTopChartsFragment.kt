package com.gitlab.visutrb.androsstore.ui.main.fragment

import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gitlab.visutrb.androsstore.proto.DocV2
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.api.GooglePlayStoreApi
import com.gitlab.visutrb.androsstore.ui.BaseFragment
import com.gitlab.visutrb.androsstore.ui.appdetails.AppDetailsIntentHelper
import com.gitlab.visutrb.androsstore.ui.main.adapter.TopChartRvAdapter
import kotlinx.android.synthetic.main.content_top_chart.view.*
import kotlinx.android.synthetic.main.fragment_top_charts.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

abstract class BaseTopChartsFragment(private val topChartType: TopChartType) : BaseFragment() {

    @Inject lateinit var googlePlayStoreApi: GooglePlayStoreApi

    private val topFreeAdt by lazy { TopChartRvAdapter() }
    private val topPaidAdt by lazy { TopChartRvAdapter() }
    private val topGrossingAdt by lazy { TopChartRvAdapter() }

    protected abstract val topFreeTitle: String
    protected abstract val topPaidTitle: String
    protected abstract val topGrossingTitle: String

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_top_charts, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        fetchContentsAsync()
    }

    private fun initViews() {
        topFreeAdt.onItemSelected = { doc, iconView -> onItemSelected(doc, iconView) }
        topPaidAdt.onItemSelected = { doc, iconView -> onItemSelected(doc, iconView) }
        topGrossingAdt.onItemSelected = { doc, iconView -> onItemSelected(doc, iconView) }

        topFree.topChartRv.apply {
            adapter = topFreeAdt
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }

        topPaid.topChartRv.apply {
            adapter = topPaidAdt
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }

        topGrossing.topChartRv.apply {
            adapter = topGrossingAdt
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }
    }

    private fun fetchContentsAsync() = launch {
        topChartSkeleton?.startShimmer()

        val topFreeCat: GooglePlayStoreApi.TopChartCategory
        val topPaidCat: GooglePlayStoreApi.TopChartCategory
        val topGrossingCat: GooglePlayStoreApi.TopChartCategory
        when (topChartType) {
            TopChartType.APPLICATION -> {
                topFreeCat = GooglePlayStoreApi.TopChartCategory.TOP_FREE_APPS
                topPaidCat = GooglePlayStoreApi.TopChartCategory.TOP_PAID_APPS
                topGrossingCat = GooglePlayStoreApi.TopChartCategory.TOP_GROSSING_APPS
            }
            TopChartType.GAME -> {
                topFreeCat = GooglePlayStoreApi.TopChartCategory.TOP_FREE_GAMES
                topPaidCat = GooglePlayStoreApi.TopChartCategory.TOP_PAID_GAMES
                topGrossingCat = GooglePlayStoreApi.TopChartCategory.TOP_GROSSING_GAMES
            }
        }

        var topFreeChart = listOf<DocV2>()
        var topPaidChart = listOf<DocV2>()
        var topGrossingChart = listOf<DocV2>()
        withContext(IO) {
            with(googlePlayStoreApi) {
                topFreeChart = getTopChart(topFreeCat)
                topPaidChart = getTopChart(topPaidCat)
                topGrossingChart = getTopChart(topGrossingCat)
            }
        }
        topFreeAdt.addAll(topFreeChart)
        topPaidAdt.addAll(topPaidChart)
        topGrossingAdt.addAll(topGrossingChart)

        topChartSkeleton?.stopShimmer()
        showContents()
    }

    private fun showContents() {
        topFree.topChartTitleTv.text = topFreeTitle
        topPaid.topChartTitleTv.text = topPaidTitle
        topGrossing.topChartTitleTv.text = topGrossingTitle
        topChartSkeleton.visibility = View.GONE
        topChartsContainer.visibility = View.VISIBLE
        topChartsLayout.removeViewAt(0)
    }

    private fun onItemSelected(doc: DocV2, iconView: View) {
        val intent = AppDetailsIntentHelper.newIntent(activity!!, doc)
        val sceneTransitionOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
            activity!!,
            iconView,
            getString(R.string.transition_name_app_icon)
        )
        startActivity(intent, sceneTransitionOptions.toBundle())
    }

    enum class TopChartType { APPLICATION, GAME }
}
