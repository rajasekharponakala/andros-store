package com.gitlab.visutrb.androsstore.ui.appdetails

import android.util.Log
import android.webkit.ConsoleMessage
import android.webkit.WebChromeClient

class LoggableWebChromeClient : WebChromeClient() {

    override fun onConsoleMessage(consoleMessage: ConsoleMessage?): Boolean {
        consoleMessage ?: return false
        val message = consoleMessage.message()
        val level = consoleMessage.messageLevel()
        when (level) {
            ConsoleMessage.MessageLevel.ERROR -> Log.e(TAG, message)
            ConsoleMessage.MessageLevel.WARNING -> Log.w(TAG, message)
            ConsoleMessage.MessageLevel.DEBUG -> Log.d(TAG, message)
            ConsoleMessage.MessageLevel.LOG -> Log.v(TAG, message)
            ConsoleMessage.MessageLevel.TIP -> Log.v(TAG, message)
            null -> return false
        }
        return true
    }

    companion object {
        private const val TAG = "JavascriptConsole"
    }
}
