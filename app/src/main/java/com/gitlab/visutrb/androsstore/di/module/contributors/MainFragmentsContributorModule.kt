package com.gitlab.visutrb.androsstore.di.module.contributors

import com.gitlab.visutrb.androsstore.di.scope.FragmentScope
import com.gitlab.visutrb.androsstore.ui.main.fragment.ExploreFragment
import com.gitlab.visutrb.androsstore.ui.main.fragment.GamesFragment
import com.gitlab.visutrb.androsstore.ui.main.fragment.MyAppsFragment
import com.gitlab.visutrb.androsstore.ui.main.fragment.SettingsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class MainFragmentsContributorModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun exploreFragment(): ExploreFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun gamesFragment(): GamesFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun myAppsFragment(): MyAppsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun settingsFragment(): SettingsFragment
}
