package com.gitlab.visutrb.androsstore.ui.search

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.gitlab.visutrb.androsstore.proto.DocV2
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.api.GooglePlayStoreApi
import com.gitlab.visutrb.androsstore.ui.BaseActivity
import com.gitlab.visutrb.androsstore.ui.appdetails.AppDetailsIntentHelper
import com.gitlab.visutrb.androsstore.ui.search.adapter.SearchResultsRvAdapter
import kotlinx.android.synthetic.main.activity_search_results.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

class SearchResultsActivity : BaseActivity() {

    private lateinit var query: String
    private var relevantCluster: DocV2? = null

    private val searchResultsAdt by lazy { SearchResultsRvAdapter() }

    @Inject lateinit var googlePlayStoreApi: GooglePlayStoreApi

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_results)

        query = intent.getStringExtra(EXT_SEARCH_QUERY)

        initViews()
        searchAsync()
    }

    private fun initViews() {
        searchResultsAdt.onItemSelected = { doc, iconView -> onItemSelected(doc, iconView) }

        searchResultsRv.apply {
            adapter = searchResultsAdt
            layoutManager = LinearLayoutManager(this@SearchResultsActivity)
            addItemDecoration(
                DividerItemDecoration(
                    this@SearchResultsActivity,
                    LinearLayoutManager.VERTICAL
                )
            )
        }
    }

    private fun searchAsync() = launch {
        searchPb.visibility = View.VISIBLE
        searchResultsRv.visibility = View.GONE

        val searchResultsClusters = withContext(IO) { googlePlayStoreApi.search(query) }

        relevantCluster = searchResultsClusters.find { it.docid.contains("search-results-cluster") }

        searchResultsClusters.forEach { searchResultsAdt.addAll(it.childList) }

        searchPb.visibility = View.GONE
        searchResultsRv.visibility = View.VISIBLE
    }

    private fun onItemSelected(doc: DocV2, iconView: View) {
        val intent = AppDetailsIntentHelper.newIntent(this, doc)
        val transitionOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
            this,
            iconView,
            getString(R.string.transition_name_app_icon)
        )
        startActivity(intent, transitionOptions.toBundle())
    }

    companion object {
        const val EXT_SEARCH_QUERY = "search_query"

        fun newIntent(context: Context, query: String): Intent {
            return Intent(context, SearchResultsActivity::class.java).also {
                it.putExtra(EXT_SEARCH_QUERY, query)
            }
        }
    }
}
