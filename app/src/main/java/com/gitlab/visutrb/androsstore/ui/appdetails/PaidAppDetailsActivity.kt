package com.gitlab.visutrb.androsstore.ui.appdetails

import android.app.Dialog
import android.content.Context
import android.content.Intent
import com.gitlab.visutrb.androsstore.proto.DocV2
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.api.exception.RequestFailureException
import kotlinx.android.synthetic.main.dialog_play_store.view.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PaidAppDetailsActivity : BaseAppDetailsActivity() {

    private lateinit var dialog: Dialog

    override fun purchaseAndDownloadAsync() = launch(Main) {
        try {
            showDownloadProgress()
            withContext(IO) { googlePlayStoreApi.acquire(doc) }
        } catch (e: RequestFailureException) {
        }

        val appDeliveryData = withContext(IO) { googlePlayStoreApi.deliver(doc) }.appDeliveryData
        if (appDeliveryData.hasDownloadUrl()) {
            startDownload(appDeliveryData)
        } else {
            showPlayStoreDialog()
            hideDownloadProgress()
        }
    }

    private fun showPlayStoreDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_play_store, null)
        view.playStoreWebView.apply {
            onPurchaseCancelled = { dialog.dismiss() }
            onPurchaseCompleted = { purchaseAndDownloadAsync(); dialog.dismiss() }
        }

        dialog = Dialog(this)
        dialog.setContentView(view)
        dialog.show()

        view.playStoreWebView.loadDetailsPage(doc.docid)
    }

    companion object {
        fun newIntent(context: Context, doc: DocV2): Intent {
            return Intent(context, PaidAppDetailsActivity::class.java).also {
                it.putExtra(EXT_DOC, doc.toByteArray())
            }
        }
    }
}
