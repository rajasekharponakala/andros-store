package com.gitlab.visutrb.androsstore.ui.appdetails

import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.widget.TextViewCompat
import android.support.v7.graphics.Palette
import android.support.v7.widget.LinearLayoutManager
import android.text.Html
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomViewTarget
import com.bumptech.glide.request.transition.Transition
import com.gitlab.visutrb.androsstore.proto.AndroidAppDeliveryData
import com.gitlab.visutrb.androsstore.proto.DetailsResponse
import com.gitlab.visutrb.androsstore.proto.DocV2
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.api.GooglePlayStoreApi
import com.gitlab.visutrb.androsstore.api.exception.RequestFailureException
import com.gitlab.visutrb.androsstore.service.DownloadManagerService
import com.gitlab.visutrb.androsstore.ui.BaseActivity
import com.gitlab.visutrb.androsstore.ui.appdetails.adapter.ScreenshotRvAdapter
import com.gitlab.visutrb.androsstore.util.FileSizeUtil
import com.gitlab.visutrb.androsstore.util.PackageHelper
import kotlinx.android.synthetic.main.activity_app_details.*
import kotlinx.android.synthetic.main.content_app_details.*
import kotlinx.android.synthetic.main.content_app_details_content_rating.*
import kotlinx.android.synthetic.main.content_app_details_download_count.*
import kotlinx.android.synthetic.main.content_app_details_download_size.*
import kotlinx.android.synthetic.main.content_app_details_get_app_controls.view.*
import kotlinx.android.synthetic.main.content_app_details_local_app_controls.view.*
import kotlinx.android.synthetic.main.content_app_details_star_rating.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.DecimalFormat
import java.text.NumberFormat
import javax.inject.Inject
import kotlin.math.roundToInt

abstract class BaseAppDetailsActivity : BaseActivity() {

    @Inject lateinit var googlePlayStoreApi: GooglePlayStoreApi

    protected lateinit var doc: DocV2

    private val appBannerTarget by lazy { AppBannerTarget() }
    private val screenshotAdt by lazy { ScreenshotRvAdapter() }

    private var downloadManagerSvc: DownloadManagerService? = null
    private val serviceConnection by lazy { DownloadManagerServiceConnection() }
    private val downloadManEventReceiver by lazy { DownloadManagerEventReceiver() }

    private val pkgInstallerEventReceiver by lazy { PackageInstallerEventReceiver() }

    private var hasDownloadUrl = false
    private var isPurchased = false

    private val lbm
        get() = LocalBroadcastManager.getInstance(applicationContext)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_details)

        doc = DocV2.parseFrom(intent.getByteArrayExtra(EXT_DOC))

        initViews()
        initStatusBar()
        showInitialDetails()
        fetchAppDetailsAsync(doc.docid)

        bindDownloadManagerService()
        registerPackageInstallerEventReceiver()
    }

    override fun onResume() {
        super.onResume()
        registerDownloadManagerEventReceiver()
    }

    override fun onPause() {
        super.onPause()
        unbindDownloadManagerService()
        unregisterDownloadManagerEventReceiver()
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterPackageInstallerEventReceiver()
    }


    private fun initViews() {
        appDetailsCollapsingToolbar.apply {
            setExpandedTitleColor(ContextCompat.getColor(context, android.R.color.transparent))
            setContentScrimColor(ContextCompat.getColor(context, R.color.colorPrimary))
        }

        appDetailsScreenshotsRv.apply {
            adapter = screenshotAdt
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        }

        appDetailsCancelDownloadBtn.setOnClickListener {
            downloadManagerSvc?.removeFromDownloadQueue(doc)
        }

        TextViewCompat.setAutoSizeTextTypeWithDefaults(
            appDetailsStarRatingTv,
            TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM
        )

        TextViewCompat.setAutoSizeTextTypeWithDefaults(
            appDetailsDownloadCountTv,
            TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM
        )

        TextViewCompat.setAutoSizeTextTypeWithDefaults(
            appDetailsDownloadSizeTv,
            TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM
        )
    }

    private fun initStatusBar() = window.apply {
        if (Build.VERSION.SDK_INT >= 21) {
            addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            statusBarColor = ContextCompat.getColor(context, android.R.color.transparent)
        }
    }

    private fun showInitialDetails() {
        appDetailsTitleTv.text = doc.title
        appDetailsCreatorTv.text = doc.creator

        val iconImageUrl = doc.imageList.find { it.imageType == 4 }?.imageUrl
        val bannerImageUrl = doc.imageList.find { it.imageType == 2 }?.imageUrl

        Glide.with(this).load(iconImageUrl).into(appDetailsIconImv)
        Glide.with(this).asBitmap().load(bannerImageUrl).into(appBannerTarget)
    }

    private fun fetchAppDetailsAsync(id: String) = launch {
        appDetailsProgressLayout.visibility = View.VISIBLE
        try {
            val detailsResponse = withContext(IO) { googlePlayStoreApi.getAppDetails(id) }
            hasDownloadUrl = withContext(IO) {
                try {
                    googlePlayStoreApi.deliver(doc)
                } catch (e: RequestFailureException) {
                    null
                }
            }?.appDeliveryData?.hasDownloadUrl() ?: false
            showFullDetailsAsync(detailsResponse)
        } catch (e: RequestFailureException) {
            Toast.makeText(
                this@BaseAppDetailsActivity,
                "This application does not exist in Play Store",
                Toast.LENGTH_SHORT
            ).show()
            finish()
        }
    }

    private fun showFullDetailsAsync(detailsResponse: DetailsResponse) = launch(IO) {
        doc = detailsResponse.docV2

        withContext(Main) { showInitialDetails() }

        val discoveryBadge = detailsResponse.getDiscoveryBadge(0)
        val offer = doc.getOffer(0)

        val starRating = DecimalFormat("#.#").format(doc.aggregateRating.starRating)
        val ratingsCount = NumberFormat.getInstance(resources.configuration.locale)
            .format(doc.aggregateRating.ratingsCount)

        val downloadCount = discoveryBadge.downloadCount
        val downloadUnits = discoveryBadge.downloadUnits

        val descriptionHtml = doc.descriptionHtml
        val screenshots = doc.imageList.filter { it.imageType == 1 || it.imageType == 3 }

        val contentRatingTitle = doc.annotations.contentRating.title
        val contentRatingIconUrl = doc.annotations.contentRating.image.imageUrl

        val installationSize =
            FileSizeUtil.getHumanReadableSize(doc.details.appDetails.installationSize)

        val localInstallationDetails = getLocalInstallationDetails(
            doc.docid,
            doc.details.appDetails.versionCode
        )

        withContext(Main) {
            appDetailsStarRatingTv.text = starRating
            appDetailsRatingsCountTv.text = ratingsCount
            appDetailsDownloadCountTv.text =
                if (downloadUnits.isBlank()) downloadCount.toString()
                else "$downloadCount $downloadUnits"
            appDetailsContentRatingTv.text = contentRatingTitle
            appDetailsDownloadSizeTv.text = installationSize
            appDetailsDescTv.text = Html.fromHtml(descriptionHtml)
            screenshotAdt.addAll(screenshots)

            Glide.with(this@BaseAppDetailsActivity)
                .load(contentRatingIconUrl)
                .into(appDetailsContentRatingImv)

            appDetailsDescToggleBtn.setOnClickListener { toggleAppDescription() }
            appDetailsDescTv.setOnClickListener { toggleAppDescription() }

            localInstallationDetails?.let { showLocalAppControls(it) }
                ?: showGetAppControls(offer.micros, offer.formattedAmount)

            appDetailsProgressLayout.removeViewAt(0)
            appDetailsProgressLayout.visibility = View.GONE
            appDetailsContentsGroup.visibility = View.VISIBLE
        }
    }

    private fun toggleAppDescription() {
        val appDescExpanded =
            (appDetailsDescTv.maxLines > resources.getInteger(R.integer.app_details_short_details_max_lines))
        if (!appDescExpanded) {
            appDetailsDescTv.maxLines = Integer.MAX_VALUE
            appDetailsDescToggleBtn.text = getString(R.string.app_details_view_less_details)
        } else {
            appDetailsDescTv.maxLines =
                resources.getInteger(R.integer.app_details_short_details_max_lines)
            appDetailsDescToggleBtn.text = getString(R.string.app_details_view_more_details)
        }
    }

    private fun showLocalAppControls(localInstallationDetails: LocalInstallationDetails) {
        val view = layoutInflater.inflate(
            R.layout.content_app_details_local_app_controls,
            appDetailsControlsLayout,
            false
        )

        view.appDetailsLaunchOrUpdateBtn.apply {
            if (localInstallationDetails.isUpdatable) {
                text = getString(R.string.app_details_update)
                setOnClickListener { purchaseAndDownloadAsync() }
            } else if (localInstallationDetails.launchIntent != null) {
                text = getString(R.string.app_details_launch)
                setOnClickListener { startActivity(localInstallationDetails.launchIntent) }
            } else {
                visibility = View.GONE
            }
        }

        view.appDetailsUninstallBtn.setOnClickListener {
            PackageHelper.getUninstallIntent(doc.docid).also { startActivity(it) }
        }

        appDetailsControlsLayout.apply {
            removeAllViews()
            addView(view)
            visibility = View.VISIBLE
        }
    }

    private fun showGetAppControls(price: Long, formattedAmount: String) {
        val view = layoutInflater.inflate(
            R.layout.content_app_details_get_app_controls,
            appDetailsControlsLayout,
            false
        )

        val isDownloaded = PackageHelper.isDownloaded(doc.docid, this)
        view.appDetailsGetAppBtn.apply {
            if (isDownloaded) {
                text = getString(R.string.app_details_install)
                setOnClickListener {
                    PackageHelper.getInstallIntent(
                        doc.docid,
                        this@BaseAppDetailsActivity
                    ).also { startActivity(it) }
                }
            } else {
                text = when {
                    (price == 0L) || hasDownloadUrl -> getString(R.string.app_details_download)
                    else -> formattedAmount
                }
                setOnClickListener {
                    purchaseAndDownloadAsync()
                }
            }
        }

        appDetailsControlsLayout.apply {
            removeAllViews()
            addView(view)
            visibility = View.VISIBLE
        }
    }

    protected fun showDownloadProgress(indefinite: Boolean = true) {
        appDetailsControlsLayout.visibility = View.GONE
        appDetailsProgressLayout.visibility = View.VISIBLE
        appDetailsDownloadProgressLayout.visibility = View.VISIBLE
        appDetailsDownloadProgress.isIndeterminate = indefinite
    }

    protected fun hideDownloadProgress() {
        appDetailsControlsLayout.visibility = View.VISIBLE
        appDetailsProgressLayout.visibility = View.GONE
        appDetailsDownloadProgressLayout.visibility = View.GONE
        appDetailsDownloadProgress.isIndeterminate = false
    }

    protected open fun purchaseAndDownloadAsync() = launch(Main) {
        try {
            showDownloadProgress()
            val deliveryResponse = withContext(IO) {
                googlePlayStoreApi.acquire(doc)
                googlePlayStoreApi.deliver(doc)
            }
            startDownload(deliveryResponse.appDeliveryData)
            hideDownloadProgress()
        } catch (e: Exception) {
            Toast.makeText(
                this@BaseAppDetailsActivity,
                "Cannot download application",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    protected fun startDownload(appDeliveryData: AndroidAppDeliveryData) {
        downloadManagerSvc?.addToDownloadQueue(doc, appDeliveryData)
    }

    private fun onDownloadProgressChange(intent: Intent) {
        if (!matchDocId(intent)) return
        val isIndeterminate = intent.getBooleanExtra(
            DownloadManagerService.EXTRA_PROGRESS_INDETERMINATE,
            false
        )
        val progress = intent.getFloatExtra(DownloadManagerService.EXTRA_PROGRESS, 0F)
        val percent = (progress * 100).roundToInt()
        appDetailsDownloadProgress.apply {
            this.isIndeterminate = isIndeterminate
            this.progress = percent
            max = 100
            visibility = View.VISIBLE
        }
        appDetailsControlsLayout.visibility = View.GONE
        appDetailsProgressLayout.visibility = View.VISIBLE
        appDetailsDownloadProgressLayout.visibility = View.VISIBLE
        isPurchased = true
    }

    private fun onDownloadFinished(intent: Intent) {
        val offer = doc.getOffer(0)
        showGetAppControls(offer.micros, offer.formattedAmount)
        hideDownloadProgress()
    }

    private fun onDownloadInterrupted() {
        hideDownloadProgress()
    }

    private fun onDownloadFailed() {
        hideDownloadProgress()
        Toast.makeText(this, "Download failed", Toast.LENGTH_SHORT).show()
    }

    protected fun getLocalInstallationDetails(
        packageId: String,
        versionCode: Int
    ): LocalInstallationDetails? {
        return try {
            val packageInfo = packageManager.getPackageInfo(packageId, 0)
            val isUpdatable = packageInfo.versionCode < versionCode
            val launchIntent = packageManager.getLaunchIntentForPackage(packageId)
            LocalInstallationDetails(isUpdatable, launchIntent)
        } catch (e: PackageManager.NameNotFoundException) {
            null
        }
    }

    private fun bindDownloadManagerService() {
        if (downloadManagerSvc == null) {
            val intent = Intent(applicationContext, DownloadManagerService::class.java)
            startService(intent)
            bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE)
        }
    }

    private fun unbindDownloadManagerService() {
        try {
            unbindService(serviceConnection)
        } catch (e: Exception) {
            Log.i(TAG, "Service stopped before unbinding.")
        }
    }

    private fun registerDownloadManagerEventReceiver() {
        lbm.registerReceiver(downloadManEventReceiver, IntentFilter().apply {
            addAction(DownloadManagerService.ACTION_DOWNLOAD_PROGRESS_CHANGE)
            addAction(DownloadManagerService.ACTION_DOWNLOAD_FINISHED)
            addAction(DownloadManagerService.ACTION_DOWNLOAD_INTERRUPTED)
            addAction(DownloadManagerService.ACTION_DOWNLOAD_FINISHED)
        })
    }

    private fun unregisterDownloadManagerEventReceiver() {
        lbm.unregisterReceiver(downloadManEventReceiver)
    }

    private fun registerPackageInstallerEventReceiver() {
        registerReceiver(pkgInstallerEventReceiver, IntentFilter().apply {
            addAction(Intent.ACTION_PACKAGE_ADDED)
            addAction(Intent.ACTION_PACKAGE_REMOVED)
            addAction(Intent.ACTION_PACKAGE_FULLY_REMOVED)
            addDataScheme("package")
        })
    }

    private fun unregisterPackageInstallerEventReceiver() {
        unregisterReceiver(pkgInstallerEventReceiver)
    }

    private fun matchDocId(intent: Intent): Boolean {
        val docId = intent.getStringExtra(DownloadManagerService.EXTRA_DOC_ID)
        return docId == doc.docid
    }

    private inner class AppBannerTarget : CustomViewTarget<ImageView, Bitmap>(appDetailsBannerImv) {

        override fun onLoadFailed(errorDrawable: Drawable?) {
        }

        override fun onResourceCleared(placeholder: Drawable?) {
        }

        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
            view.setImageBitmap(resource)
            val swatch = Palette.from(resource).generate().dominantSwatch ?: return
            appDetailsCollapsingToolbar.apply {
                setContentScrimColor(swatch.rgb)
                setStatusBarScrimColor(swatch.rgb)
            }
        }
    }

    private inner class DownloadManagerServiceConnection : ServiceConnection {

        override fun onServiceConnected(name: ComponentName?, binder: IBinder?) {
            Log.i(TAG, "Connected to download manager.")
            downloadManagerSvc = (binder as DownloadManagerService.Binder).getService()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.i(TAG, "Disconnected from download manager.")
            downloadManagerSvc = null
        }
    }

    private inner class DownloadManagerEventReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            val docId = intent?.getStringExtra(DownloadManagerService.EXTRA_DOC_ID) ?: return
            if (docId != doc.docid) return
            when (intent.action) {
                DownloadManagerService.ACTION_DOWNLOAD_PROGRESS_CHANGE ->
                    onDownloadProgressChange(intent)
                DownloadManagerService.ACTION_DOWNLOAD_FINISHED -> onDownloadFinished(intent)
                DownloadManagerService.ACTION_DOWNLOAD_INTERRUPTED -> onDownloadInterrupted()
                DownloadManagerService.ACTION_DOWNLOAD_FAILED -> onDownloadFailed()
            }
        }
    }

    private inner class PackageInstallerEventReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            val packageName = intent?.data?.schemeSpecificPart ?: return
            if (packageName != doc.docid) return
            when (intent.action) {
                Intent.ACTION_PACKAGE_ADDED -> showLocalAppControls(
                    getLocalInstallationDetails(
                        doc.docid,
                        doc.details.appDetails.versionCode
                    )!!
                )
                Intent.ACTION_PACKAGE_FULLY_REMOVED -> showGetAppControls(
                    doc.getOffer(0).micros,
                    doc.getOffer(0).formattedAmount
                )
            }
        }
    }

    protected data class LocalInstallationDetails(
        val isUpdatable: Boolean,
        val launchIntent: Intent?
    )

    companion object {
        const val EXT_DOC = "extra_doc"

        private const val TAG = "BaseAppDetailsActivity"
    }
}
