package com.gitlab.visutrb.androsstore.ui.appdetails

import android.content.Context
import android.content.Intent
import com.gitlab.visutrb.androsstore.proto.DocV2

class FreeAppDetailsActivity : BaseAppDetailsActivity() {

    companion object {
        fun newIntent(context: Context, doc: DocV2): Intent {
            return Intent(context, FreeAppDetailsActivity::class.java).also {
                it.putExtra(EXT_DOC, doc.toByteArray())
            }
        }
    }
}
