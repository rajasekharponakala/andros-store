package com.gitlab.visutrb.androsstore.ui.login

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.gitlab.visutrb.androsstore.R
import com.gitlab.visutrb.androsstore.api.GoogleAuthApi
import com.gitlab.visutrb.androsstore.api.GoogleCheckinApi
import com.gitlab.visutrb.androsstore.api.exception.NeedsBrowserException
import com.gitlab.visutrb.androsstore.api.exception.RequestFailureException
import com.gitlab.visutrb.androsstore.ui.BaseActivity
import com.gitlab.visutrb.androsstore.ui.main.MainActivity
import com.gitlab.visutrb.androsstore.util.AppPrefs
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.IOException
import javax.inject.Inject

class LoginActivity : BaseActivity() {

    @Inject lateinit var googleAuthApi: GoogleAuthApi
    @Inject lateinit var googleCheckinApi: GoogleCheckinApi

    @Inject lateinit var appPrefs: AppPrefs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initViews()
    }

    private fun initViews() {
        continueBtn.setOnClickListener { loginAsync() }
    }

    private fun loginAsync() = launch {
        val email = emailEt.text.toString()
        val password = passwordEt.text.toString()
        showProgress()
        try {
            withContext(IO) {
                val ac2dmAuthResponse = googleAuthApi.login(
                    email = email,
                    password = password,
                    service = GoogleAuthApi.Service.AC2DM
                )
                googleAuthApi.login(
                    email = email,
                    token = ac2dmAuthResponse.token,
                    service = GoogleAuthApi.Service.ANDROID_MARKET
                )
                val checkinResponse = googleCheckinApi.checkin()
                val androidMarketAuthResponse = googleAuthApi.login(
                    email = email,
                    token = ac2dmAuthResponse.token,
                    deviceId = "",
                    service = GoogleAuthApi.Service.ANDROID_MARKET
                )
                appPrefs.apply {
                    isCheckedIn = true
                    isLoggedIn = true
                    authToken = androidMarketAuthResponse.auth
                    checkinToken = checkinResponse.deviceCheckinConsistencyToken
                    deviceId = googleCheckinApi.getDeviceIdFromCheckinResponse(checkinResponse)
                }
            }
            startActivity(MainActivity.newIntent(this@LoginActivity))
            finish()
            return@launch
        } catch (e: NeedsBrowserException) {
            Toast.makeText(
                this@LoginActivity,
                "Uh oh! Looks like you have 2FA enabled. To login with 2FA enabled account, please use app password.",
                Toast.LENGTH_SHORT
            ).show()
        } catch (e: RequestFailureException) {
            Toast.makeText(
                this@LoginActivity,
                "Incorrect username or password",
                Toast.LENGTH_SHORT
            ).show()
        } catch (e: IOException) {
            Toast.makeText(
                this@LoginActivity,
                "No Internet connection. Please try again later",
                Toast.LENGTH_SHORT
            ).show()
        }
        hideProgress()
    }

    private fun showProgress() {
        loginInputsGroup.visibility = View.INVISIBLE
        loginProgressGroup.visibility = View.VISIBLE
    }

    private fun hideProgress() {
        loginProgressGroup.visibility = View.GONE
        loginInputsGroup.visibility = View.VISIBLE
    }
}
