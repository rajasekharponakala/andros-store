package com.gitlab.visutrb.androsstore.api

import com.gitlab.visutrb.androsstore.api.exception.RequestFailureException
import okhttp3.*

abstract class BaseGoogleApi(protected val client: OkHttpClient) {

    protected abstract val userAgentString: String

    protected fun httpGet(url: String, headers: Headers? = null) =
        httpGet(HttpUrl.parse(url)!!, headers)

    protected fun httpGet(url: HttpUrl, headers: Headers? = null): Response {
        val request = Request.Builder().apply {
            get()
            url(url)
            headers?.let { headers(it) }
        }.build()
        return executeRequest(request)
    }

    protected fun httpPost(url: String, body: RequestBody?, headers: Headers? = null) =
        httpPost(HttpUrl.parse(url)!!, body, headers)

    protected fun httpPost(url: HttpUrl, body: RequestBody?, headers: Headers? = null): Response {
        val request = Request.Builder().apply {
            post(body ?: RequestBody.create(null, ""))
            url(url)
            headers?.let { headers(it) }
        }.build()
        return executeRequest(request)
    }

    protected fun executeRequest(request: Request): Response {
        val response = client.newCall(request).execute()
        if (!response.isSuccessful) {
            val statusCode = response.code()
            val message = response.message()
            val body = response.body()?.string()
            throw RequestFailureException(statusCode, message, body)
        }
        return response
    }

    companion object {
        const val BASE_URL = "https://android.clients.google.com"

        const val MIME_TYPE_PROTOBUF = "application/x-protobuf"

        const val GSF_SIGNATURE = "38918a453d07199354f8b19af05ec6562ced5788"
        const val ENCODED_TARGETS = "CAEShwLBlYEGpgrRAkIC2AMBFZIJpAjmA+0EkgHpCpgB8gKXATLAAuADx" +
                "AXNBrgDAQLPBa4DzxPRCM0TAacEnAIwIK4OowKtA7AC9AOvDYsE3wadBZwCmgNupwSIAhgCGv0DWQ" +
                "EzYgGyCc8ChAOcAk4BYboCXTLzAqUJpQOGAU/XBQrGB7IBqwJjMdwGvwHdAQ6xAcMDxgHXAhwHWE7" +
                "FAToE/AL/AXyZBEaCAs4FBU2iA5sBpAQ4euYBJfoKZq0EiwIBAowF5wM5KkKOAaQBsgKgAp8EROwD" +
                "Qhi1AZIC6AbGAgGsBPcCmQUpASHsCqABezY7AqkIkAMLAjV6uwGbA/wCB6kDBtYDUULuDxrXBxEBA" +
                "QIBAguxAQFSnQKfAQIDBGsBBgEvgNPPAoK7sQMBAQMCBAkICQECCAQBAwQCAgQFGRQDAwMCDAEBAQ" +
                "UCAQLEAQEWBA/nAX0vAh0BCpABDDMXASEKFA8GByI3hAEODBZNCVh/EREYAQOLAYEBFBAjCBFwWhk" +
                "CD18L3wGEAoQBBDhMAgEBigEZGAsrFhYFBwEqZQICJShzFCct9AQwAQ43EgY6S40BtAFZQo4BHgEm" +
                "CyQQJi9b1AFiA3cJAQowAWVIeaoBDAMsZJ4BBIEBdFBFDLUBE4oBzAIFBQMGzQEEARA1oAEd+wJgN" +
                "S7OAQ1yqwEgiwM/+wImlwMeQ60ChAZ24wWBBA72AxZznAFVNgEBNQEJPAHeBREPEA7QAV6VAYYGKx" +
                "ipAS8BIQEHAiAIEh0HpgM5GkYrlQN1UlUYJbUCAQEYFQEPxgsNEIABK1yEAsoBAQIBwwHbAjYBATE" +
                "IAQKrAucDQfUCwgF1ARMtiwGyA0qbAQYSKRZEOBQrVUXiAoUCCgsImgEIbCegAfcBtwH8AxXmAnls" +
                "KwTyAmIVGSKxARgBBgwLjQEaAgKEBbUBDQYiAYEBnwEyHh14gAE2SqUB5gEM5gEmSSIFIkcBAxCsA" +
                "gQbAzfcAT2dA18LGP4BMyOAARtaArkBDIgBJnaLAXBYATA3AgO0AaECAyExFD4DqgGaA2ujAil6mg" +
                "MKCAYcwwEHgAEDQaUBtQJPEpECHIoBE8YBlQMUFAIDATEyTh8DygGDAVABAVqHAcQB0wFEAQd9cX2" +
                "JAXcOggEOETICAgLaARgBLZ0BlAImXAURWx9qA5EBBiwrEYABMAIBAQkB2ALJAh07kgF9pgEytwH3" +
                "ATgGBAMEAQQC0AJ+VRZEQ2hRQhgjcwQCBAMEAQa2AW0gRSc4FQ6VAwdxqAKCAdsBDwMINS7WAgInH" +
                "U9YMBUnDxLOAccBNicBOwYDMBZ8V3ECWVg1LAHZAXMGBAIFBgQDBAIFBgQCQmgQbAevBGoVE5cCIQ" +
                "IFngER1wI0hAI3BgYEAwQBGE0CAQMSKRNHEQEu1wJcNwE48gPXAQEs/wJ/EQoDbVsrBAIEAgQDBTg" +
                "BNAUupQEwFC8giAIKK3AVFh3SASUL1AEZnwEhDhoHB80CcoABLQICAwYGBQgRkAEbIwuDATgEIz4C" +
                "KMcBDzoKBxbmAQcrIrUBE3OpBBkOhwEyDZ4BTAKcARQUnwF22gEUEgQLGo8Ca8EBJhQGAQ8tAVQFq" +
                "QElCGKMAQYysgFGNgyuARCaAYkB/AEBFM0BLhKyAQIDAykIDQoSBBRDIQjGASwSSggCR08QBQME"
    }
}
