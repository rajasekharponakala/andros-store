package com.gitlab.visutrb.androsstore.api

import com.gitlab.visutrb.androsstore.api.util.DeviceConfig
import com.gitlab.visutrb.androsstore.proto.*
import okhttp3.Headers
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import java.util.*

class GoogleCheckinApi(
    client: OkHttpClient,
    private val deviceConfig: DeviceConfig
) : BaseGoogleApi(client) {

    override val userAgentString: String
        get() = with(deviceConfig) { "Android-Checkin/2.0 ($device $buildId)" }

    fun checkin(): AndroidCheckinResponse {
        val checkinRequestProto = generateCheckinRequestProto()
        val requestBody = RequestBody.create(
            MediaType.parse(MIME_TYPE_PROTOBUF),
            checkinRequestProto.toByteArray()
        )
        val response = httpPost(CHECKIN_URL, requestBody, generateHeaders())
        return AndroidCheckinResponse.parseFrom(response.body()!!.bytes())
    }

    fun getDeviceIdFromCheckinResponse(checkinResponse: AndroidCheckinResponse) =
        checkinResponse.androidId.toString(16)

    private fun generateCheckinRequestProto() = with(deviceConfig) {
        val unixTime = System.currentTimeMillis()
        val androidBuildProto = AndroidBuildProto.newBuilder()
            .setId(fingerprint)
            .setProduct(product)
            .setCarrier(carrierBranding)
            .setRadio(radioVersion)
            .setBootloader(bootloaderVersion)
            .setClient("android-google")
            .setTimestamp(unixTime)
            .setDevice(device)
            .setSdkVersion(sdkVersionInt)
            .setModel(model)
            .setManufacturer(manufacturer)
            .setBuildProduct(product)
            .setOtaInstalled(false)
            .build()
        val androidCheckinProto = AndroidCheckinProto.newBuilder()
            .setBuild(androidBuildProto)
            .setLastCheckinMsec(0)
            .setCellOperator(networkOperator)
            .setSimOperator(simOperator)
            .setRoaming("mobile-notroaming")
            .setUserNumber(0)
            .build()
        val deviceConfigurationProto = DeviceConfigurationProto.newBuilder()
            .setTouchScreen(touchScreenConfiguration)
            .setKeyboard(keyboardConfiguration)
            .setNavigation(navigationConfiguration)
            .setScreenLayout(screenLayoutConfiguration)
            .setHasHardKeyboard(!isHardwareKeyboardHidden)
            .setHasFiveWayNavigation(navigationConfiguration != 1)
            .setScreenDensity(screenDensity)
            .setGlEsVersion(glEsVersionInt)
            .addAllSystemSharedLibrary(sharedLibraries)
            .addAllSystemAvailableFeature(systemFeatures)
            .addAllNativePlatform(supportedAbis)
            .setScreenWidth(screenWidth)
            .setScreenHeight(screenHeight)
            .addAllSystemSupportedLocale(supportedLocales)
            .addAllGlExtension(glExtensions)
            .build()

        AndroidCheckinRequest.newBuilder()
            .setId(0)
            .setCheckin(androidCheckinProto)
            .setLocale(locale)
            .setTimeZone(TimeZone.getDefault().id)
            .setVersion(3)
            .setDeviceConfiguration(deviceConfigurationProto)
            .setFragment(0)
            .build()
    }

    private fun generateHeaders() = Headers.Builder().apply {
        add("user-agent", userAgentString)
    }.build()

    companion object {
        private const val CHECKIN_URL = "$BASE_URL/checkin"
    }
}
